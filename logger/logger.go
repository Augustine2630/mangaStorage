package logger

import (
	"sync"

	"github.com/sirupsen/logrus"
)

// Logger is the singleton instance of logrus.Logger
type Logger struct {
	*logrus.Logger
}

var (
	once     sync.Once
	instance *Logger
)

// GetLogger returns the singleton instance of Logger
func GetLogger() *Logger {
	once.Do(func() {
		log := logrus.New()
		log.SetReportCaller(true)
		//log.SetFormatter(&logrus.JSONFormatter{})
		instance = &Logger{log}
	})
	return instance
}
