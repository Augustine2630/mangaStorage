package router

import (
	"github.com/gofiber/fiber/v2"
	"mangaStorage/api/controller"
	"mangaStorage/bootstrap"
	"mangaStorage/usecase"
)

func setupMangaFileRouter(app *fiber.App) {

	fileController := controller.MangaFileController{
		MangaFileUseCase: usecase.NewMangaFileUseCase(*bootstrap.Apps.MinioClient),
	}
	fileApi := app.Group("/api/v1/file/manga")
	fileApi.Get("/rename", fileController.RenameFiles)
	fileApi.Get("/:title", fileController.GetFileByName)
}
