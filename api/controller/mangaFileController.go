package controller

import (
	"github.com/gofiber/fiber/v2"
	"io"
	"mangaStorage/domain"
	"net/http"
	"strings"
)

type MangaFileController struct {
	MangaFileUseCase domain.MangaFileUseCase
}

func (m MangaFileController) GetFileByName(c *fiber.Ctx) error {

	object, err := m.MangaFileUseCase.GetFileByNameAndPage(c.Params("title"), c.Query("name"), c.Query("page"))
	defer object.Close()
	if err != nil {
		if strings.Contains(err.Error(), "not found") {
			return c.SendStatus(404)
		}
		return err
	}

	c.Set("Content-Disposition", "inline; filename="+c.Query("name"))
	c.Set("Content-Type", "image/jpeg;image/png") // or the appropriate MIME type for your file

	// Stream the object to the client
	c.Status(http.StatusOK)
	if _, err := io.Copy(c, object); err != nil {
		return c.Status(http.StatusInternalServerError).SendString(err.Error())
	}
	return c.SendStatus(200)
}

func (m MangaFileController) RenameFiles(c *fiber.Ctx) error {
	err := m.MangaFileUseCase.RenameFiles()
	if err != nil {
		return err
	}
	return nil
}
