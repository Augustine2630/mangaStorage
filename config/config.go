package config

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"mangaStorage/logger"
	"os"
)

type Yaml struct {
	Server struct {
		Port int
	}
	Logging struct {
		Level string
	}
	S3 struct {
		Host string
		Port string
	} `mapstructure:"s3"`
}

var (
	Config Yaml
	log    = logger.GetLogger()
)

func SetupConfig() {
	viper.SetConfigName("config")                   // name of config file (without extension)
	viper.SetConfigType("yaml")                     // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath(os.Getenv("CONFIG_FOLDER")) // optionally look for config in the working directory
	err := viper.ReadInConfig()                     // Find and read the config file
	if err != nil {                                 // Handle errors reading the config file
		log.Fatal("Fatal error config file: %s \n", err)
	}
	err = viper.Unmarshal(&Config)
	if err != nil {
		log.Fatal("Cannot unmarshall config to struct")
	}
	format, err := convertToLogrusFormat(Config.Logging.Level)
	if err != nil {
		log.Fatal(err)
	}
	log.SetLevel(format)
}

func convertToLogrusFormat(level string) (logrus.Level, error) {
	switch level {
	case "INFO":
		return logrus.InfoLevel, nil
	case "DEBUG":
		return logrus.DebugLevel, nil
	case "ERROR":
		return logrus.ErrorLevel, nil
	case "FATAL":
		return logrus.FatalLevel, nil
	case "PANIC":
		return logrus.PanicLevel, nil
	default:
		return 0, fmt.Errorf("not valid logger level")
	}
}
