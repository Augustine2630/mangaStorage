package usecase

import (
	"fmt"
	"github.com/minio/minio-go"
	"mangaStorage/domain"
	"mangaStorage/logger"
	"strings"
)

type mangaFileUseCase struct {
	minioClient minio.Client
}

func NewMangaFileUseCase(client minio.Client) domain.MangaFileUseCase {
	return &mangaFileUseCase{minioClient: client}
}

var (
	log = logger.GetLogger()
)

func (m mangaFileUseCase) GetFileByNameAndPage(title string, name string, page string) (*minio.Object, error) {
	bucket := "manga"
	path := title + "/" + name + "/" + page
	log.Infof("getting %s", bucket+"/"+path)

	doneCh := make(chan struct{})
	defer close(doneCh)
	var searchObject string
	for object := range m.minioClient.ListObjects(bucket, path, true, doneCh) {
		if object.Err != nil {
			return nil, object.Err
		}
		key := object.Key[strings.LastIndex(object.Key, "/")+1:]

		if strings.Split(key, ".")[0] == page {
			searchObject = object.Key
		}
	}
	if searchObject == "" {
		return nil, fmt.Errorf("object not found")
	}
	object, err := m.minioClient.GetObject(bucket, searchObject, minio.GetObjectOptions{})
	if err != nil {
		return nil, err
	}
	return object, nil
}

func (m mangaFileUseCase) RenameFiles() error {
	bucketName := "manga"
	prefix := "attack-on-titan/aot-118/"

	doneCh := make(chan struct{})
	defer close(doneCh)

	objectCh := m.minioClient.ListObjectsV2(bucketName, prefix, true, doneCh)
	for object := range objectCh {
		if object.Err != nil {
			log.Error(object.Err)
		}
		var newName string
		if strings.Contains(object.Key, "_fan-naruto.ru") {
			newName = strings.Replace(object.Key, "_fan-naruto.ru", "", 1)
		}
		src := minio.NewSourceInfo(bucketName, object.Key, nil)
		dst, _ := minio.NewDestinationInfo(bucketName, newName, nil, map[string]string{
			"x-amz-meta-my-custom-header": "custom-value",
		})

		// Copy the object
		err := m.minioClient.CopyObject(dst, src)
		if err != nil {
			log.Error(err)
		}

		err = m.minioClient.RemoveObject(bucketName, object.Key)
		if err != nil {
			return err
		}

		log.Printf("Renamed object from %s to %s\n", object.Key, newName)
	}
	return nil
}
