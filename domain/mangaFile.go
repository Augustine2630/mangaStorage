package domain

import "github.com/minio/minio-go"

type MangaFileUseCase interface {
	GetFileByNameAndPage(title string, name string, page string) (*minio.Object, error)
	RenameFiles() error
}
