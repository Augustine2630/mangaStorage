package bootstrap

import (
	"github.com/minio/minio-go"
	"mangaStorage/config"
	"mangaStorage/logger"
)

type Application struct {
	MinioClient *minio.Client
}

var Apps Application

var log = logger.GetLogger()

func App() Application {
	config.SetupConfig()
	client := SetupMinioClient()
	return Application{MinioClient: client}
}
