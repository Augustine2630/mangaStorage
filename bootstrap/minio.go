package bootstrap

import (
	"github.com/minio/minio-go"
	"mangaStorage/config"
	"os"
)

func SetupMinioClient() *minio.Client {
	endpoint := config.Config.S3.Host + ":" + config.Config.S3.Port
	accessKeyID := os.Getenv("AccessKey")
	secretAccessKey := os.Getenv("SecretKey")

	// Initialize minio client object.
	minioClient, err := minio.New(endpoint, accessKeyID, secretAccessKey, false)
	if err != nil {
		log.Fatalln(err)
	}

	buckets, err := minioClient.ListBuckets()
	if err != nil {
		log.Fatalln(err)
	}

	// Print out the bucket names.
	for _, bucket := range buckets {
		log.Debugf("* %s\n", bucket.Name)
	}

	return minioClient
}
