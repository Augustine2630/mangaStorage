package main

import (
	"github.com/gofiber/fiber/v2"
	"mangaStorage/api/router"
	"mangaStorage/bootstrap"
	"mangaStorage/config"
	"mangaStorage/logger"
	"strconv"
)

var log = logger.GetLogger()

func main() {
	app := fiber.New()

	bootstrap.Apps = bootstrap.App()

	router.SetupRouter(app)

	log.Fatal(app.Listen("0.0.0.0:" + strconv.Itoa(config.Config.Server.Port)))
}
